$(document).ready(() => {

    const EMPTY_SLOT = 0;
    const HUMAN_PLAYER_SYMBOL = "X";
    const MACHINE_PLAYER_SYMBOL = "O";
    const HEIGHT = 6;
    const WIDTH = 7;
    let HUMAN = 1;
    let MACHINE = 2;
    let currentPlayer = HUMAN;

    let nn_input = Array(HEIGHT * WIDTH * 3).fill().map(x => EMPTY_SLOT);

    let brain = prepareNet();
    brain.value_net.fromJSON(JSON.parse(netAsJSON));

    // Notwendig, da ansonsten die ersten Züge random wären, da das Netz noch nicht ausreichend temporalen Speicher hat
    // for (let i = 0; i < 6; i++) {
    //     brain.forward(nn_input);
    // }

    let board = Array(HEIGHT * WIDTH).fill().map(x => EMPTY_SLOT);
    for (let x = 0; x < WIDTH; x++) {
        $(".gamefield").append(`<div class="colHeader" id="${x}"></div>`);
    }

    for (let y = HEIGHT - 1; y >= 0; y--) {
        for (let x = 0; x < WIDTH; x++) {
            $(".gamefield").append(`<div class="coin" id="${y}-${x}"></div>`);
        }
    }

    let gameOver = false;
    $(".coin").bind('click', function (e) {
        if (!gameOver) {
            if (currentPlayer === HUMAN) {
                let y = parseInt($(this).attr("id").split("-")[0]);
                let x = parseInt($(this).attr("id").split("-")[1]);
                if (insertCoin(x)) {
                    paintBoard(x);

                    currentPlayer = MACHINE;
                }
            }
            if (currentPlayer === MACHINE) {
                let done = false;
                while (!done) {
                    let c = -1;
                    nn_input = nn_input.map(v => {
                        c++;

                        if (c % 3 == 0) {
                            return board[Math.floor(c / 3)] === EMPTY_SLOT ? 1 : 0
                        } else if (c % 3 == 1) {
                            return board[Math.floor(c / 3)] === MACHINE ? 1 : 0;
                        } else {
                            return board[Math.floor(c / 3)] === HUMAN ? 1 : 0;
                        }
                    });

                    let result = brain.forward(nn_input);
                    if (insertCoin(result.action)) {
                        done = true;
                        paintBoard(result.action);
                        // testBoardNeuronMapping(nn_input);
                        $(`div.colHeader`).css("backgroundColor", "white");

                        for (let i = 0; i < WIDTH; i++) {
                            $(`div.colHeader#${i}`).html(parseInt(result.values.w[i] * 100) / 100);
                        }
                        $(`div.colHeader#${result.action}`).css("backgroundColor", "#BEE599");

                    } else {
                        console.log("invalid move by NN (Col: " + result.action + ") :-/ --> repeat")
                    }
                }
                currentPlayer = HUMAN;
            }
        }
    });

    $(document).on("click", "button.newGame", function () {
        gameOver = false;
        $(this).hide();
        board = Array(HEIGHT * WIDTH).fill().map(x => EMPTY_SLOT);
        paintBoard(0);
        currentPlayer = HUMAN;
        $(".message").hide(100);
        for (let i = 0; i < WIDTH; i++) {
            $(`div.colHeader#${i}`).html("");
            $(`div.colHeader#${i}`).css("backgroundColor", "white");
        }
    });

    function paintBoard(action) {
        for (let y = 0; y < HEIGHT; y++) {
            for (let x = 0; x < WIDTH; x++) {
                // let symbol = "y:" + y + " x:" + x;
                let symbol = "";
                if (board[y * WIDTH + x] === MACHINE) {
                    symbol = MACHINE_PLAYER_SYMBOL;
                } else if (board[y * WIDTH + x] === HUMAN) {
                    symbol = HUMAN_PLAYER_SYMBOL;
                }
                // $(`.coin#${y}-${x}`).html("");
                $(`.coin#${y}-${x}`).html(`<span class="${symbol}"></span>`);
            }
        }
        let win = winCheck(action);
        if (win == HUMAN) {
            gameOver = true;
            showMessage("Sieg gegen die Maschine!!!")
        }
        if (win == MACHINE) {
            gameOver = true;
            showMessage("Skynet gewinnt! :-/")
        }
        if (checkTie()) {
            gameOver = true;
            showMessage("unentschieden!");
        }
        if (gameOver) {
            $("button.newGame").show();
        }
    }

    function showMessage(msg) {
        $(".message").text(msg);
        $(".message").fadeIn(1000);
    }

    function insertCoin(col) {
        for (let row = 0; row < HEIGHT; row++) {
            if (board[row * WIDTH + col] === EMPTY_SLOT) {
                board[row * WIDTH + col] = currentPlayer;
                return true;
            }
        }
        return false;
    }

    function checkTie() {
        for (let c = 0; c < WIDTH - 1; c++) {
            if (getCoin(HEIGHT - 1, c) === EMPTY_SLOT) {
                return false;
            }
        }
        return true;
    }

    function winCheck(col) {
        let row = 0;
        let found = false;

        while (!found && row < HEIGHT - 1) {
            if (getCoin(row, col) === EMPTY_SLOT) {
                found = true;
            } else {
                row++;
            }
        }
        row--;

        let player = getCoin(row, col);

        // Check Horizontal
        let horizontalCounter = 1;
        for (let c = col - 1; c >= 0 && getCoin(row, c) === player; c--) {
            horizontalCounter++;
        }
        for (let c = col + 1; c < WIDTH && getCoin(row, c) == player; c++) {
            horizontalCounter++;
        }
        if (horizontalCounter === 4) {
            return player;
        }

        // Check diagonal topLeft-bottomRight
        let vertical1 = 1;
        //to the top-Left
        for (let c = col - 1, r = row + 1; c >= 0 && r < HEIGHT && getCoin(r, c) === player; c-- , r++) {
            vertical1++;
        }
        //to the bottom-right
        for (let c = col + 1, r = row - 1; c < WIDTH && r >= 0 && getCoin(r, c) === player; c++ , r--) {
            vertical1++;
        }
        if (vertical1 === 4) {
            return player;
        }

        // Check diagonal bottomLeft-topRight
        let vertical2 = 1;
        //to the top-Right
        for (let c = col + 1, r = row + 1; c < WIDTH && r < HEIGHT && getCoin(r, c) === player; c++ , r++) {
            vertical2++;
        }
        //to the bottom-left
        for (let c = col - 1, r = row - 1; c >= 0 && r >= 0 && getCoin(r, c) === player; c-- , r--) {
            vertical2++;
        }
        if (vertical2 === 4) {
            return player;
        }

        // bottom
        if (row > 2) {
            if (getCoin(row - 1, col) === player
                && getCoin(row - 1, col) === getCoin(row - 2, col)
                && getCoin(row - 2, col) == getCoin(row - 3, col)) {
                return player;
            }
        }


        return EMPTY_SLOT; // no winner
    }


    function getCoin(row, col) {
        return board[row * WIDTH + col];
    }

    function testBoardNeuronMapping(nn_input) {
        console.log(JSON.stringify(board));

        console.log(JSON.stringify("nn_input --> " + nn_input));
        let testBoard = Array(HEIGHT * WIDTH).fill().map(x => EMPTY_SLOT);
        for (let i = 0; i < nn_input.length; i++) {
            let boardIndex = Math.floor(i / 3);
            if (i % 3 === 0 && nn_input[i] == 1) {
                testBoard[boardIndex] = EMPTY_SLOT;
            } else if (i % 3 == 1 && nn_input[i] == 1) {
                testBoard[boardIndex] = HUMAN;
            } else if (i % 3 == 2 && nn_input[i] == 1) {
                testBoard[boardIndex] = MACHINE;
            }
        }

        console.log("");
        for (let row = HEIGHT - 1; row >= 0; row--) { // iterate rows, bottom to top
            let line = "";
            for (let col = 0; col < WIDTH; col++) {
                let char = col != 0 ? "|" : " ";
                if (testBoard[row * WIDTH + col] === HUMAN) {
                    char += HUMAN_PLAYER_SYMBOL;
                } else if (testBoard[row * WIDTH + col] === MACHINE) {
                    char += MACHINE_PLAYER_SYMBOL
                } else {
                    char += " ";
                }
                line += char;
            }
            // console.log("---------------------")
            console.log(line);
        }




    }


});




function prepareNet() {
    let num_inputs = 7 * 6 * 3; // Länge und Breite von Vier Gewinnt * 3 Stati für Leer, X, O
    let num_actions = 7; // 7 Spalten
    let temporal_window = 0; // amount of temporal memory. 0 = agent lives in-the-moment :)
    let network_size = num_inputs * temporal_window + num_actions * temporal_window + num_inputs;

    // the value function network computes a value of taking any of the possible actions
    // given an input state. Here we specify one explicitly the hard way
    // but user could also equivalently instead use opt.hidden_layer_sizes = [20,20]
    // to just insert simple relu hidden layers.
    var layer_defs = [];
    layer_defs.push({ type: 'input', out_sx: 1, out_sy: 1, out_depth: network_size });
    layer_defs.push({ type: 'fc', num_neurons: 700, activation: 'sigmoid' });
    layer_defs.push({ type: 'fc', num_neurons: 700, activation: 'sigmoid' });
    layer_defs.push({ type: 'fc', num_neurons: 700, activation: 'sigmoid' });
    layer_defs.push({ type: 'fc', num_neurons: 700, activation: 'sigmoid' });
    layer_defs.push({ type: 'fc', num_neurons: 700, activation: 'sigmoid' });

    // war passabel, wenn man es mit dem Netz hier drüber ausführt... warum auch immer. Vermutlich Einbildung
    // da die Layer beim Laden eines Netzes nicht genutzt werden (glaube ich)
    // layer_defs.push({ type: 'fc', num_neurons: 150, activation: 'relu' });
    // layer_defs.push({ type: 'fc', num_neurons: 50, activation: 'relu' });
    // layer_defs.push({ type: 'fc', num_neurons: 50, activation: 'relu' });
    // layer_defs.push({ type: 'fc', num_neurons: 50, activation: 'relu' });
    // layer_defs.push({ type: 'fc', num_neurons: 50, activation: 'relu' });
    // layer_defs.push({ type: 'fc', num_neurons: 30, activation: 'relu' });

    layer_defs.push({ type: 'regression', num_neurons: num_actions });

    // options for the Temporal Difference learner that trains the above net
    // by backpropping the temporal difference learning rule.
    let tdtrainer_options = { learning_rate: 0.01, momentum: 0.0, batch_size: 64, l2_decay: 0.01 };

    let opt = {};
    opt.temporal_window = temporal_window;
    opt.experience_size = 30000;
    opt.start_learn_threshold = 1000;
    opt.gamma = 0.7;
    opt.learning_steps_total = 200000;
    opt.learning_steps_burnin = 3000;
    opt.epsilon_min = 0.05;
    opt.epsilon_test_time = 0.01;
    opt.layer_defs = layer_defs;
    opt.tdtrainer_options = tdtrainer_options;
    let b = new deepqlearn.Brain(num_inputs, num_actions, opt);
    b.learning = false;
    return b;
}
