const convnetjs = require("./convnet.js");
const deepqlearn = require("./deepqlearn.js");
let fs = require('fs');

const HEIGHT = 6;
const WIDTH = 7;

const PRINT_NET_ITERATION = 1000;
const PRINT_NET_MIN_ITERATION = 1000;
const EMPTY_SLOT = 0;
const X_PLAYER = 1;
const X_PLAYER_SYMBOL = "X";
const O_PLAYER = 2;
const O_PLAYER_SYMBOL = "O";
let currentPlayerNumber = X_PLAYER;
let enemyPlayerNumber = O_PLAYER;

let winCounter = [0, 0, 0];

let WIN_REWARD = 30;
const PRINT_CONSOLE = false;

let board = Array(HEIGHT * WIDTH).fill().map(x => EMPTY_SLOT);
let nn_input = Array(HEIGHT * WIDTH * 3).fill().map(x => EMPTY_SLOT);

let iterationCounter = 0;
let player_X = prepareNet();
let player_O = prepareNet();


while (true) {
    currentPlayerNumber = X_PLAYER;
    enemyPlayerNumber = O_PLAYER;
    playerMove(player_X, player_O);
    currentPlayerNumber = O_PLAYER;
    enemyPlayerNumber = X_PLAYER;
    playerMove(player_O, player_X);
}

function playerMove(currentPlayer, otherPlayer) {
    let c = -1;
    nn_input = nn_input.map(v => {
        c++;
        if (c % 3 == 0) {
            return board[Math.floor(c / 3)] === EMPTY_SLOT ? 1 : 0
        } else if (c % 3 == 1) {
            return board[Math.floor(c / 3)] === currentPlayerNumber ? 1 : 0; // mein Stein
        } else {
            return board[Math.floor(c / 3)] === enemyPlayerNumber ? 1 : 0; // Gegnerstein
        }
    });
    var action = currentPlayer.forward(nn_input);
    while (!insertCoin(action)) {
        currentPlayer.backward(-2);
        action = currentPlayer.forward(nn_input);
    }
    let reward = calcReward(action)
    currentPlayer.backward(reward);
    if (reward === WIN_REWARD) {
        winCounter[currentPlayerNumber]++;
        otherPlayer.backward(-WIN_REWARD); // verloren
    }

    // printBoard();
}

function printBoard() {
    console.log("");
    for (let row = HEIGHT - 1; row >= 0; row--) { // iterate rows, bottom to top
        let line = "";
        for (let col = 0; col <= WIDTH; col++) {
            let char = col != 0 ? "|" : " ";
            if (board[row * WIDTH + col] === X_PLAYER) {
                char += "X";
            } else if (board[row * WIDTH + col] === O_PLAYER) {
                char += "O"
            } else {
                char += " ";
            }
            line += char;
        }
        // console.log("---------------------")
        console.log(line);
    }
}

function insertCoin(col) {
    for (let row = 0; row < HEIGHT; row++) {
        if (board[row * WIDTH + col] === EMPTY_SLOT) {
            board[row * WIDTH + col] = currentPlayerNumber;
            return true;
        }
    }
    return false;
}

function calcReward(action) {
    // liegt ein Gewinn vor, dann Reward = 10, sonst 0
    let winner = winCheck(action);
    if (winner === currentPlayerNumber) {
        newRound();
        return WIN_REWARD;
    }
    if (checkTie()) {
        newRound();
        return 0;
    }
    // erfolgreiche Zug gibt eine kleine Belohnung
    return 1;
}

function newRound() {
    board = board.map(c => 0);
    iterationCounter++;
    if (iterationCounter % PRINT_NET_ITERATION === 0 && iterationCounter > PRINT_NET_MIN_ITERATION) {
        // console.log(`Iteration: ${iterationCounter} winCounter --> x:${winCounter[X_PLAYER]} y:${winCounter[O_PLAYER]}`);
        if (winCounter[X_PLAYER] > winCounter[O_PLAYER]) {
            let xBrainAsJson = JSON.stringify(player_X.value_net.toJSON());
            let winDiff = winCounter[X_PLAYER] - winCounter[O_PLAYER];
            fs.writeFileSync(`./nets/X_net_iteration${iterationCounter}_winDiff_${winDiff}_date${getDate()}.json`, JSON.stringify(xBrainAsJson));
        } else {
            let oBrainAsJson = JSON.stringify(player_O.value_net.toJSON());
            let winDiff = winCounter[O_PLAYER] - winCounter[X_PLAYER];
            fs.writeFileSync(`./nets/O_net_iteration${iterationCounter}_winDiff_${winDiff}_date${getDate()}.json`, JSON.stringify(oBrainAsJson));
        }
        winCounter = [0, 0, 0];
    }
    console.log(`Iteration: ${iterationCounter} `);
}

// TODO: Wincheck ist oben und oben rechts teils buggy. In Spalte 4 die oberen vier Vertikal wird nicht erkannnt
// Diagonal nach oben rechts ist ebenfalls buggy ganz oben rechts in der Ecke
function winCheck(col) {

    let row = 0;
    let found = false;

    while (!found && row < HEIGHT - 1) {
        if (getCoin(row, col) === EMPTY_SLOT) {
            found = true;
        } else {
            row++;
        }
    }
    row--;

    let player = getCoin(row, col);

    // Check Horizontal
    let horizontalCounter = 1;
    for (let c = col - 1; c >= 0 && getCoin(row, c) === player; c--) {
        horizontalCounter++;
    }
    for (let c = col + 1; c < WIDTH && getCoin(row, c) == player; c++) {
        horizontalCounter++;
    }
    if (horizontalCounter === 4) {
        return player;
    }

    // Check diagonal topLeft-bottomRight
    let vertical1 = 1;
    //to the top-Left
    for (let c = col - 1, r = row + 1; c >= 0 && r < HEIGHT && getCoin(r, c) === player; c-- , r++) {
        vertical1++;
    }
    //to the bottom-right
    for (let c = col + 1, r = row - 1; c < WIDTH && r >= 0 && getCoin(r, c) === player; c++ , r--) {
        vertical1++;
    }
    if (vertical1 === 4) {
        return player;
    }

    // Check diagonal bottomLeft-topRight
    let vertical2 = 1;
    //to the top-Right
    for (let c = col + 1, r = row + 1; c < WIDTH && r < HEIGHT && getCoin(r, c) === player; c++ , r++) {
        vertical2++;
    }
    //to the bottom-left
    for (let c = col - 1, r = row - 1; c >= 0 && r >= 0 && getCoin(r, c) === player; c-- , r--) {
        vertical2++;
    }
    if (vertical2 === 4) {
        return player;
    }

    // bottom
    if (row > 2) {
        if (getCoin(row - 1, col) === player
            && getCoin(row - 1, col) === getCoin(row - 2, col)
            && getCoin(row - 2, col) == getCoin(row - 3, col)) {
            return player;
        }
    }
    return EMPTY_SLOT; // no winner
}

function checkTie() {
    for (let c = 0; c < WIDTH - 1; c++) {
        if (getCoin(HEIGHT - 1, c) === EMPTY_SLOT) {
            return false;
        }
    }
    return true;
}

function getCoin(row, col) {
    return board[row * WIDTH + col];
}


function prepareNet() {
    let num_inputs = 7 * 6 * 3; // Länge * Breite von Vier Gewinnt * Status Leer, X, O
    let num_actions = 7; // 7 Spalten
    // let temporal_window = 1; // amount of temporal memory. 0 = agent lives in-the-moment :)
    // let network_size = num_inputs * temporal_window + num_actions * temporal_window + num_inputs;

    // the value function network computes a value of taking any of the possible actions
    // given an input state. Here we specify one explicitly the hard way
    // but user could also equivalently instead use opt.hidden_layer_sizes = [20,20]
    // to just insert simple relu hidden layers.
    var layer_defs = [];
    layer_defs.push({ type: 'input', out_sx: 1, out_sy: 1, out_depth: num_inputs });
    layer_defs.push({ type: 'fc', num_neurons: 67, activation: 'maxout' });




    layer_defs.push({ type: 'regression', num_neurons: num_actions });

    // options for the Temporal Difference learner that trains the above net
    // by backpropping the temporal difference learning rule.

    // hier war mal batch_size: 64 , evtl. wieder reinnehmen
    let tdtrainer_options = { learning_rate: 0.001, momentum: 0.0, batch_size: 32, l2_decay: 0.01 };

    let opt = {};
    opt.temporal_window = 8;
    opt.experience_size = 100000;
    opt.start_learn_threshold = 15000;
    opt.gamma = 0.8;
    opt.learning_steps_total = 200000;
    opt.learning_steps_burnin = 200;
    opt.epsilon_min = 0.05;
    opt.epsilon_test_time = 0.05;
    opt.layer_defs = layer_defs;
    opt.tdtrainer_options = tdtrainer_options;

    return new deepqlearn.Brain(num_inputs, num_actions, opt);
}

function currentPlayerSymbol() {
    return currentPlayerNumber === X_PLAYER ? X_PLAYER_SYMBOL : O_PLAYER_SYMBOL;
}

function getDate() {
    var a = new Date();
    var months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
    var year = a.getFullYear();
    var month = months[a.getMonth()];
    var date = a.getDate();
    var hour = a.getHours();
    var min = a.getMinutes();
    var sec = a.getSeconds();
    var time = date + ' ' + month + ' ' + year + ' ' + hour + '_' + min + '_' + sec;
    return time;
}